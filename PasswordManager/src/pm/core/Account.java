package pm.core;

import java.io.*;

public class Account implements Serializable
{
	private static final long serialVersionUID = 1345798387125040416L;

	public static final String DEFAULT_TITLE = "Untitled account";
	public static final int ENTRY_COUNT = 22;

	protected String textTitle = Account.DEFAULT_TITLE;
	protected String textDescription = null;
	protected String valueID = null;
	protected String valuePassword = null;
	protected String value1stMail = null;
	protected String value2ndMail = null;
	protected String value3rdMail = null;
	protected String value1stTel = null;
	protected String value2ndTel = null;
	protected String value3rdTel = null;
	protected String value1stSecretQuestion = null;
	protected String value2ndSecretQuestion = null;
	protected String value3rdSecretQuestion = null;
	protected String value1stSecretAnswer = null;
	protected String value2ndSecretAnswer = null;
	protected String value3rdSecretAnswer = null;
	protected String valueFirstName = null;
	protected String valueLastName = null;
	protected String valueDisplayName = null;
	protected String valueSex = null;
	protected String valueBirth = null;
	protected String value1stCustomField = null;
	protected String value2ndCustomField = null;
	protected String value3rdCustomField = null;

	public Account() {}

	public Account(String t,String d,String[] a)
	{
		if(a == null) throw new IllegalArgumentException("Null array");
		if(a.length != 22) throw new IllegalArgumentException("Invalid array");
		this.setTitle(t);
		this.setDescription(d);

		this.valueID = a[0];
		this.valuePassword = a[1];
		this.value1stMail = a[2];
		this.value2ndMail = a[3];
		this.value3rdMail = a[4];
		this.value1stTel = a[5];
		this.value2ndTel = a[6];
		this.value3rdTel = a[7];
		this.value1stSecretQuestion = a[8];
		this.value2ndSecretQuestion = a[9];
		this.value3rdSecretQuestion = a[10];
		this.value1stSecretAnswer = a[11];
		this.value2ndSecretAnswer = a[12];
		this.value3rdSecretAnswer = a[13];
		this.valueFirstName = a[14];
		this.valueLastName = a[15];
		this.valueDisplayName = a[16];
		this.valueSex = a[17];
		this.valueBirth = a[18];
		this.value1stCustomField = a[19];
		this.value2ndCustomField = a[20];
		this.value3rdCustomField = a[21];
	}

	public int getEntryCount()
	{
		return Account.ENTRY_COUNT;
	}

	public void setValueAt(int index,String v)
	{
		switch(index) {
		case 0:
			this.valueID = v;
			break;
		case 1:
			this.valuePassword = v;
			break;
		case 2:
			this.value1stMail = v;
			break;
		case 3:
			this.value2ndMail = v;
			break;
		case 4:
			this.value3rdMail = v;
			break;
		case 5:
			this.value1stTel = v;
			break;
		case 6:
			this.value2ndTel = v;
			break;
		case 7:
			this.value3rdTel = v;
			break;
		case 8:
			this.value1stSecretQuestion = v;
			break;
		case 9:
			this.value2ndSecretQuestion = v;
			break;
		case 10:
			this.value3rdSecretQuestion = v;
			break;
		case 11:
			this.value1stSecretAnswer = v;
			break;
		case 12:
			this.value2ndSecretAnswer = v;
			break;
		case 13:
			this.value3rdSecretAnswer = v;
			break;
		case 14:
			this.valueFirstName = v;
			break;
		case 15:
			this.valueLastName = v;
			break;
		case 16:
			this.valueDisplayName = v;
			break;
		case 17:
			this.valueSex = v;
			break;
		case 18:
			this.valueBirth = v;
			break;
		case 19:
			this.value1stCustomField = v;
			break;
		case 20:
			this.value2ndCustomField = v;
			break;
		case 21:
			this.value3rdCustomField = v;
			break;
		default:
			break;
		}
	}

	public String[] toArray()
	{
		String[] array = new String[] {
				this.valueID,
				this.valuePassword,
				this.value1stMail,
				this.value2ndMail,
				this.value3rdMail,
				this.value1stTel,
				this.value2ndTel,
				this.value3rdTel,
				this.value1stSecretQuestion,
				this.value2ndSecretQuestion,
				this.value3rdSecretQuestion,
				this.value1stSecretAnswer,
				this.value2ndSecretAnswer,
				this.value3rdSecretAnswer,
				this.valueFirstName,
				this.valueLastName,
				this.valueDisplayName,
				this.valueSex,
				this.valueBirth,
				this.value1stCustomField,
				this.value2ndCustomField,
				this.value3rdCustomField,
		};

		return array;
	}

	public String getTitle()
	{
		return this.textTitle;
	}

	public String getDescription()
	{
		return this.textDescription;
	}

	public String getValue1stMail()
	{
		return value1stMail;
	}

	public String getValue2ndMail()
	{
		return value2ndMail;
	}

	public String getValue3rdMail()
	{
		return value3rdMail;
	}

	public String getValue1stTel()
	{
		return value1stTel;
	}

	public String getValue2ndTel()
	{
		return value2ndTel;
	}

	public String getValue3rdTel()
	{
		return value3rdTel;
	}

	public String getValue1stSecretQuestion()
	{
		return value1stSecretQuestion;
	}

	public String getValue2ndSecretQuestion()
	{
		return value2ndSecretQuestion;
	}

	public String getValue3rdSecretQuestion()
	{
		return value3rdSecretQuestion;
	}

	public String getValue1stSecretAnswer()
	{
		return value1stSecretAnswer;
	}

	public String getValue2ndSecretAnswer()
	{
		return value2ndSecretAnswer;
	}

	public String getValue3rdSecretAnswer()
	{
		return value3rdSecretAnswer;
	}

	public String getValue1stCustomField()
	{
		return value1stCustomField;
	}

	public String getValue2ndCustomField()
	{
		return value2ndCustomField;
	}

	public String getValue3rdCustomField()
	{
		return value3rdCustomField;
	}

	public String getValueID()
	{
		return valueID;
	}

	public String getValueFirstName()
	{
		return valueFirstName;
	}

	public String getValueLastName()
	{
		return valueLastName;
	}

	public String getValueDisplayName()
	{
		return valueDisplayName;
	}

	public String getValueSex()
	{
		return valueSex;
	}

	public String getValuePassword()
	{
		return valuePassword;
	}

	public String getCalendarBirth()
	{
		return valueBirth;
	}

	public void setTitle(String t)
	{
		if(t == null) this.textTitle = Account.DEFAULT_TITLE;
		else if(t.isBlank()) this.textTitle = Account.DEFAULT_TITLE;
		else this.textTitle = t;
	}

	public void setDescription(String d)
	{
		this.textDescription = d;
	}

	public void setValue1stMail(String value1stMail)
	{
		this.value1stMail = value1stMail;
	}

	public void setValue2ndMail(String value2ndMail)
	{
		this.value2ndMail = value2ndMail;
	}

	public void setValue3rdMail(String value3rdMail)
	{
		this.value3rdMail = value3rdMail;
	}

	public void setValue1stTel(String value1stTel)
	{
		this.value1stTel = value1stTel;
	}

	public void setValue2ndTel(String value2ndTel)
	{
		this.value2ndTel = value2ndTel;
	}

	public void setValue3rdTel(String value3rdTel)
	{
		this.value3rdTel = value3rdTel;
	}

	public void setValue1stSecretQuestion(String value1stSecretQuestion)
	{
		this.value1stSecretQuestion = value1stSecretQuestion;
	}

	public void setValue2ndSecretQuestion(String value2ndSecretQuestion)
	{
		this.value2ndSecretQuestion = value2ndSecretQuestion;
	}

	public void setValue3rdSecretQuestion(String value3rdSecretQuestion)
	{
		this.value3rdSecretQuestion = value3rdSecretQuestion;
	}

	public void setValue1stSecretAnswer(String value1stSecretAnswer)
	{
		this.value1stSecretAnswer = value1stSecretAnswer;
	}

	public void setValue2ndSecretAnswer(String value2ndSecretAnswer)
	{
		this.value2ndSecretAnswer = value2ndSecretAnswer;
	}

	public void setValue3rdSecretAnswer(String value3rdSecretAnswer)
	{
		this.value3rdSecretAnswer = value3rdSecretAnswer;
	}

	public void setValue1stCustomField(String value1stCustomField)
	{
		this.value1stCustomField = value1stCustomField;
	}

	public void setValue2ndCustomField(String value2ndCustomField)
	{
		this.value2ndCustomField = value2ndCustomField;
	}

	public void setValue3rdCustomField(String value3rdCustomField)
	{
		this.value3rdCustomField = value3rdCustomField;
	}

	public void setValueID(String valueID)
	{
		this.valueID = valueID;
	}

	public void setValueFirstName(String valueFirstName)
	{
		this.valueFirstName = valueFirstName;
	}

	public void setValueLastName(String valueLastName)
	{
		this.valueLastName = valueLastName;
	}

	public void setValueDisplayName(String valueDisplayName)
	{
		this.valueDisplayName = valueDisplayName;
	}

	public void setValueSex(String valueSex)
	{
		this.valueSex = valueSex;
	}

	public void setValuePassword(String valuePassword)
	{
		this.valuePassword = valuePassword;
	}

	public void setValueBirth(String valueBirth)
	{
		this.valueBirth = valueBirth;
	}

	@Override
	public String toString()
	{
		return this.textTitle;
	}
}
