package pm.core;

public final class NullPerson extends Person
{
	private static final long serialVersionUID = 8117430887205797769L;

	public NullPerson()
	{
		super.textName = "Null person";
		super.textDescription = "This is null person.";
	}

	@Override
	public void setName(String n) {}

	@Override
	public void setDescription(String d) {}

	@Override
	public void addAccount(Account a) {}

	@Override
	public void removeAccount(Account a) {}

	@Override
	public void removeAccountAt(int index) {}

	@Override
	public Account getAccountAt(int index)
	{
		return null;
	}

	@Override
	public int getNumberOfAccounts()
	{
		return 0;
	}

	@Override
	public int getSize()
	{
		return 0;
	}

	@Override
	public Account getElementAt(int index)
	{
		return null;
	}
}
