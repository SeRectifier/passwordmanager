package pm.core;

import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

public class Person implements Serializable,ListModel<Account>
{
	private static final long serialVersionUID = 7589722106866524113L;

	public static final String DEFAULT_NAME = "Unnamed person";

	protected String textName = Person.DEFAULT_NAME;
	protected String textDescription = null;
	protected ArrayList<Account> listAccount = new ArrayList<Account>(0);
	protected ArrayList<ListDataListener> listListener = new ArrayList<ListDataListener>(0);

	public void fireListDataEvent()
	{
		ListDataEvent e = new ListDataEvent(this,ListDataEvent.CONTENTS_CHANGED,0,this.listAccount.size());
		for(ListDataListener listener : this.listListener) listener.contentsChanged(e);
	}

	public void setName(String n)
	{
		if(n == null) this.textName = Account.DEFAULT_TITLE;
		else if(n.isBlank()) this.textName = Account.DEFAULT_TITLE;
		else this.textName = n;
	}

	public void setDescription(String d)
	{
		this.textDescription = d;
	}

	public String getName()
	{
		return this.textName;
	}

	public String getDescription()
	{
		return this.textDescription;
	}

	public void addAccount(Account a)
	{
		if(a == null) return;
		this.listAccount.add(a);
		this.fireListDataEvent();
	}

	public void removeAccount(Account a)
	{
		if(a == null) return;
		this.listAccount.remove(a);
		this.listAccount.trimToSize();
		this.fireListDataEvent();
	}

	public void removeAccountAt(int index)
	{
		if(index < 0 || this.listAccount.size() <= index) return;
		this.listAccount.remove(index);
		this.listAccount.trimToSize();
		this.fireListDataEvent();
	}

	public Account getAccountAt(int index)
	{
		if(index < 0 || this.listAccount.size() <= index) return null;
		return this.listAccount.get(index);
	}

	public int getNumberOfAccounts()
	{
		return this.listAccount.size();
	}

	@Override
	public int getSize()
	{
		return this.listAccount.size();
	}

	@Override
	public Account getElementAt(int index)
	{
		return this.getAccountAt(index);
	}

	@Override
	public void addListDataListener(ListDataListener l)
	{
		if(l == null) return;
		this.listListener.add(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l)
	{
		this.listListener.remove(l);
	}
}
