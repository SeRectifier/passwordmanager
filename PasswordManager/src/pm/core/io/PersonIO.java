package pm.core.io;

import java.io.*;
import pm.core.*;

public class PersonIO
{
	public static void save(File f,String s,Person p) throws IOException
	{
		if(f == null) return;

		int noa = p.getNumberOfAccounts();
		KeyRing ring = new KeyRing(s);
		BufferedOutputStream streamOut = new BufferedOutputStream(new FileOutputStream(f));

		score(streamOut,p.getName());
		score(streamOut,p.getDescription());

		streamOut.write(noa);

		String[] info = null;
		for(int i=0; i<noa; i++) {
			score(streamOut,p.getAccountAt(i).getTitle());
			score(streamOut,p.getAccountAt(i).getDescription());
			info = p.getAccountAt(i).toArray();
			for(int j=0; j<info.length; j++) scoreSecretly(streamOut,ring,info[j]);
		}

		streamOut.close();
	}

	public static Person load(File f,String s) throws IOException
	{
		if(f == null) return null;

		Person p = new Person();
		KeyRing ring = new KeyRing(s);
		BufferedInputStream streamIn = new BufferedInputStream(new FileInputStream(f));

		p.setName(new String(check(streamIn)));
		p.setDescription(new String(check(streamIn)));

		int noa = streamIn.read();

		String title = null;
		String description = null;
		Account account = null;
		String[] array = new String[Account.ENTRY_COUNT];
		for(int i=0; i<noa; i++) {
			title = new String(check(streamIn));
			description = new String(check(streamIn));
			for(int j=0; j<Account.ENTRY_COUNT; j++) array[j] = new String(checkSecretly(streamIn,ring));
			account = new Account(title,description,array);
			p.addAccount(account);
		}

		streamIn.close();

		return p;
	}

	private static void score(BufferedOutputStream streamOut,String s) throws IOException
	{
		byte[] a = s == null ? "".getBytes() : s.getBytes();
		streamOut.write(a.length);
		streamOut.write(a);
	}

	private static void scoreSecretly(BufferedOutputStream streamOut,KeyRing ring,String s) throws IOException
	{
		if(ring == null) return;

		byte[] a = s == null ? "".getBytes() : s.getBytes();
		int len = a.length;
		byte[] enc = new byte[len];
		for(int i=0; i<len; i++) enc[i] = (byte) (a[i] ^ ring.get());
		streamOut.write(len);
		streamOut.write(enc);
	}

	private static byte[] check(BufferedInputStream streamIn) throws IOException
	{
		int len = streamIn.read();
		byte[] a = streamIn.readNBytes(len);
		return a;
	}

	private static byte[] checkSecretly(BufferedInputStream streamIn,KeyRing ring) throws IOException
	{
		int len = streamIn.read();
		byte[] enc = streamIn.readNBytes(len);
		byte[] a = new byte[len];
		for(int i=0; i<len; i++) a[i] = (byte) (enc[i] ^ ring.get());
		return a;
	}

	static class KeyRing
	{
		protected int count = -1;
		protected int size = 0;
		protected byte[] array = null;

		public KeyRing(String s)
		{
			if(s == null) {
				this.size = 1;
				this.array = new byte[] {0};
			}
			else if("".equals(s)) {
				this.size = 1;
				this.array = new byte[] {0};
			}
			else {
				byte[] a = s.getBytes();
				this.size = a.length;
				this.array = a;
			}
		}

		public byte get()
		{
			this.count++;
			this.count = this.count == this.size ? 0 : this.count;
			if(this.size == 1) return 0;
			else return this.array[this.count];
		}

		public byte refer(int index)
		{
			return this.array[index % this.size];
		}
	}
}
