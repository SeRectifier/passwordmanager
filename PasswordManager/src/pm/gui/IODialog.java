package pm.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class IODialog extends JDialog
{
	private static final long serialVersionUID = 3587009084286834556L;

	public static final int MODE_NONE = 0;
	public static final int MODE_LOAD = 1;
	public static final int MODE_SAVE = 2;

	protected int mode = IODialog.MODE_NONE;
	protected String path = null;
	protected String masterpass = null;

	private final JLabel labelPath = new JLabel("Path: ");
	private final JLabel labelPass = new JLabel("Master password: ");
	private final JButton buttonBrowse = new JButton("Browse...");
	private final JTextField fieldPath = new JTextField(32);
	private final JPasswordField fieldPass = new JPasswordField(32);
	private final JButton buttonCancel = new JButton("Cancel");
	private final JButton buttonDone = new JButton();

	public IODialog(JFrame owner,int mode)
	{
		super(owner,true);
		this.mode = mode;

		this.initField();
		this.initListener();
		this.initPane();

		super.pack();
		super.setResizable(false);
	}

	private void initField()
	{
		switch(this.mode) {
		case IODialog.MODE_NONE:
			super.setTitle("File");
			this.buttonDone.setText("Done");
			break;
		case IODialog.MODE_LOAD:
			super.setTitle("Load");
			this.buttonDone.setText("Load");
			break;
		case IODialog.MODE_SAVE:
			super.setTitle("Save");
			this.buttonDone.setText("Save");
			break;
		}

		this.buttonBrowse.setActionCommand("browse");
		this.buttonCancel.setActionCommand("cancel");
		this.buttonDone.setActionCommand("done");
	}

	private void initListener()
	{
		Listener listener = new Listener();
		this.buttonBrowse.addActionListener(listener);
		this.buttonCancel.addActionListener(listener);
		this.buttonDone.addActionListener(listener);
		super.addWindowListener(listener);
	}

	private void initPane()
	{
		GridBagConstraints gbc = new GridBagConstraints();
		GridBagLayout layout = new GridBagLayout();
		JPanel paneCenter = new JPanel(layout);

		gbc.gridx = gbc.gridy = 0;
		gbc.gridwidth = gbc.gridheight = 1;
		gbc.insets = new Insets(4,4,4,4);
		gbc.fill = GridBagConstraints.BOTH;

		layout.setConstraints(this.labelPath,gbc);
		paneCenter.add(this.labelPath);
		gbc.gridy = 1;
		layout.setConstraints(this.labelPass,gbc);
		paneCenter.add(this.labelPass);

		gbc.gridx = 1;
		gbc.gridy = 0;
		layout.setConstraints(this.fieldPath,gbc);
		paneCenter.add(this.fieldPath);
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		layout.setConstraints(this.fieldPass,gbc);
		paneCenter.add(this.fieldPass);

		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		layout.setConstraints(this.buttonBrowse,gbc);
		paneCenter.add(this.buttonBrowse);

		JPanel paneSouth = new JPanel(new FlowLayout());
		paneSouth.add(this.buttonCancel);
		paneSouth.add(this.buttonDone);

		super.add(paneSouth,BorderLayout.SOUTH);
		super.add(paneCenter,BorderLayout.CENTER);
	}

	public String getMasterPassword()
	{
		return this.masterpass;
	}

	public String getPath()
	{
		return this.path;
	}

	class Listener extends WindowAdapter implements ActionListener
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			IODialog.this.path = null;
			IODialog.this.masterpass = null;
			IODialog.super.dispose();
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			String command = e.getActionCommand();
			switch(command) {
			case "browse":
				if(IODialog.this.mode == IODialog.MODE_LOAD) {
					JFileChooser chooser = new JFileChooser();
					int res = chooser.showOpenDialog(IODialog.super.getContentPane());
					if(res == JFileChooser.CANCEL_OPTION) return;
					String path = chooser.getSelectedFile().getAbsolutePath();
					IODialog.this.fieldPath.setText(path);
				}
				else if(IODialog.this.mode == IODialog.MODE_SAVE) {
					JFileChooser chooser = new JFileChooser();
					int res = chooser.showSaveDialog(IODialog.super.getContentPane());
					if(res == JFileChooser.CANCEL_OPTION) return;
					String path = chooser.getSelectedFile().getAbsolutePath();
					IODialog.this.fieldPath.setText(path);
				}
				else {
					JFileChooser chooser = new JFileChooser();
					int res = chooser.showDialog(IODialog.super.getContentPane(),"Done");
					if(res == JFileChooser.CANCEL_OPTION) return;
					String path = chooser.getSelectedFile().getAbsolutePath();
					IODialog.this.fieldPath.setText(path);
				}
				break;
			case "cancel":
				IODialog.this.path = null;
				IODialog.this.masterpass = null;
				IODialog.super.dispose();
				break;
			case "done":
				IODialog.this.path = IODialog.this.fieldPath.getText();
				IODialog.this.masterpass = new String(IODialog.this.fieldPass.getPassword());
				IODialog.super.dispose();
				break;
			}
		}
	}
}
