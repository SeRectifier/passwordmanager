package pm.gui;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import java.io.*;

import javax.swing.*;
import javax.swing.event.*;

import pm.core.*;
import pm.core.io.*;

public class Master extends JFrame
{
	private static final long serialVersionUID = -2941531772859703323L;

	public static final String DEFAULT_TITLE = "Password manager 1.0(beta)";

	private Person person = new NullPerson();
	private Account accountSelected = null;
	private String masterpass = null;
	private File file = null;

	private final JList<Account> list = new JList<Account>();
	private final JButton buttonNew = new JButton("New");
	private final JButton buttonDelete = new JButton("Delete");
	private final JButton buttonDescription = new JButton("Edit...");
	private final JButton buttonScore = new JButton("Score");
	private final JLabel labelTitle = new JLabel("Title: ");
	private final JLabel labelDescription = new JLabel("Description: ");
	private final JTextField fieldTitle = new JTextField(32);
	private final JButton[] arrayClipButton = new JButton[22];
	private final JTextField[] arrayAccountField = new JTextField[22];
	private final JMenuItem[] arrayConfigMenuItem = new JMenuItem[] {
			new JMenuItem("Settings..."),
			new JMenuItem("About..."),
	};
	private final JMenuItem[] arrayFileMenuItem = new JMenuItem[] {
			new JMenuItem("New"),
			new JMenuItem("Open..."),
			new JMenuItem("Save"),
			new JMenuItem("Save as..."),
			new JMenuItem("Quit"),
	};
	private final JLabel[] arrayAccountLabel = new JLabel[] {
			new JLabel("ID: "),
			new JLabel("Password: "),
			new JLabel("1st Email: "),
			new JLabel("2nd Email: "),
			new JLabel("3rd Email: "),
			new JLabel("1st Tel: "),
			new JLabel("2nd Tel: "),
			new JLabel("3rd Tel: "),
			new JLabel("1st secret question: "),
			new JLabel("2nd secret question: "),
			new JLabel("3rd secret question: "),
			new JLabel("1st secret answer: "),
			new JLabel("2nd secret answer: "),
			new JLabel("3rd secret answer: "),
			new JLabel("First name: "),
			new JLabel("Last name: "),
			new JLabel("Display name: "),
			new JLabel("Sex: "),
			new JLabel("Birthday: "),
			new JLabel("Custom field 1: "),
			new JLabel("Custom field 2: "),
			new JLabel("Custom field 3: "),
	};

	public Master()
	{
		super(Master.DEFAULT_TITLE);

		this.initField();
		this.initListener();
		this.initPane();

		super.setSize(700,500);
		super.setResizable(true);
	}

	private void initField()
	{
		this.buttonNew.setActionCommand("function_new");
		this.buttonDelete.setActionCommand("function_delete");
		this.buttonDescription.setActionCommand("edit_description");
		this.buttonScore.setActionCommand("edit_score");
		for(int i=0; i<this.arrayAccountField.length; i++) this.arrayAccountField[i] = new JTextField();
		for(int i=0; i<this.arrayClipButton.length; i++) this.arrayClipButton[i] = new JButton("Copy to clipboard");

		this.arrayFileMenuItem[0].setActionCommand("file_new");
		this.arrayFileMenuItem[1].setActionCommand("file_load");
		this.arrayFileMenuItem[2].setActionCommand("file_save");
		this.arrayFileMenuItem[3].setActionCommand("file_saveas");
		this.arrayFileMenuItem[4].setActionCommand("file_quit");
		this.arrayConfigMenuItem[0].setActionCommand("config_settings");
		this.arrayConfigMenuItem[1].setActionCommand("config_about");

		JMenuBar bar = new JMenuBar();
		JMenu[] arrayMenu = new JMenu[] {
				new JMenu("File"),
				new JMenu("Config"),
		};

		for(JMenuItem item : this.arrayFileMenuItem) arrayMenu[0].add(item);
		for(JMenuItem item : this.arrayConfigMenuItem) arrayMenu[1].add(item);
		for(JMenu menu : arrayMenu) bar.add(menu);
		super.setJMenuBar(bar);

		this.list.setModel(this.person);

		this.setFunctionUsable(false);
		this.setFieldEditable(false);
	}

	private void initListener()
	{
		MenuItemListener listenerMenuItem = new MenuItemListener();
		FunctionListener listenerFunction = new FunctionListener();
		EditListener listenerEdit = new EditListener();
		QuitListener listenerQuit = new QuitListener();

		for(JMenuItem item : this.arrayFileMenuItem) item.addActionListener(listenerMenuItem);
		for(JMenuItem item : this.arrayConfigMenuItem) item.addActionListener(listenerMenuItem);

		for(int i=0; i<this.arrayClipButton.length; i++) 
			this.arrayClipButton[i].addActionListener(new ClipBoardListener(i));

		this.buttonNew.addActionListener(listenerFunction);
		this.buttonDelete.addActionListener(listenerFunction);
		this.list.addListSelectionListener(listenerFunction);

		this.buttonScore.addActionListener(listenerEdit);
		this.buttonDescription.addActionListener(listenerEdit);

		super.addWindowListener(listenerQuit);
	}

	private void initPane()
	{
		GridBagConstraints gbc = new GridBagConstraints();

		GridBagLayout layoutHead = new GridBagLayout();
		JPanel paneHead = new JPanel(layoutHead);

		gbc.gridwidth = gbc.gridheight = 1;
		gbc.ipadx = gbc.ipady = 2;
		gbc.insets = new Insets(4,4,4,4);
		gbc.fill = GridBagConstraints.BOTH;

		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		layoutHead.setConstraints(this.labelTitle,gbc);
		gbc.gridy = 1;
		layoutHead.setConstraints(this.labelDescription,gbc);

		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 1;
		gbc.gridy = 0;
		layoutHead.setConstraints(this.fieldTitle,gbc);
		gbc.gridy = 1;
		layoutHead.setConstraints(this.buttonDescription,gbc);

		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 2;
		layoutHead.setConstraints(this.buttonScore,gbc);

		JComponent[] arrayComp = new JComponent[] {
				this.labelTitle,
				this.labelDescription,
				this.fieldTitle,
				this.buttonDescription,
				this.buttonScore,
		};
		for(JComponent component : arrayComp) paneHead.add(component);

		GridBagLayout layoutBody = new GridBagLayout();
		JPanel paneBody = new JPanel(layoutBody);

		gbc.gridwidth = gbc.gridheight = 1;
		gbc.ipadx = gbc.ipady = 0;
		gbc.insets = new Insets(4,4,4,4);
		gbc.fill = GridBagConstraints.BOTH;

		for(int i=0; i<22; i++) {
			gbc.gridy = i;
			gbc.gridx = 0;
			gbc.weightx = 0.1;
			layoutBody.setConstraints(this.arrayAccountLabel[i],gbc);
			paneBody.add(this.arrayAccountLabel[i]);
			gbc.gridx = 1;
			gbc.weightx = 0.85;
			layoutBody.setConstraints(this.arrayAccountField[i],gbc);
			paneBody.add(this.arrayAccountField[i]);
			gbc.gridx = 2;
			gbc.weightx = 0.05;
			layoutBody.setConstraints(this.arrayClipButton[i],gbc);
			paneBody.add(this.arrayClipButton[i]);
		}

		JPanel paneContent = new JPanel(new BorderLayout());
		paneContent.add(paneHead,BorderLayout.NORTH);
		paneContent.add(paneBody,BorderLayout.CENTER);

		JScrollPane paneCenter = new JScrollPane(paneContent);
		paneCenter.getHorizontalScrollBar().setUnitIncrement(24);
		paneCenter.getVerticalScrollBar().setUnitIncrement(24);

		JPanel paneFunction = new JPanel(new GridLayout(1,4));
		paneFunction.add(this.buttonNew);
		paneFunction.add(this.buttonDelete);

		JScrollPane paneList = new JScrollPane(this.list);

		JPanel paneWest = new JPanel(new BorderLayout());
		paneWest.add(paneList,BorderLayout.CENTER);
		paneWest.add(paneFunction,BorderLayout.SOUTH);

		super.add(paneWest,BorderLayout.WEST);
		super.add(paneCenter,BorderLayout.CENTER);
	}

	public void setPerson(Person p)
	{
		this.person = p == null ? new NullPerson() : p;
		if(this.person instanceof NullPerson) {
			for(JTextField ttf : this.arrayAccountField) {
				ttf.setText(null);
				ttf.setEnabled(false);
			}
		}
		this.list.setModel(this.person);
		this.setFunctionUsable(!(this.person instanceof NullPerson));
	}

	public void setFile(File f)
	{
		this.file = f;
		if(this.file == null) super.setTitle(Master.DEFAULT_TITLE);
		else super.setTitle(Master.DEFAULT_TITLE + " - " + this.file.getAbsolutePath());
	}

	public void setAccountSelected(Account a)
	{
		if(a == null) {
			this.accountSelected = null;
			this.fieldTitle.setText(null);
			for(JTextField field : this.arrayAccountField) field.setText(null);
			this.setFieldEditable(false);
			this.list.clearSelection();
		}
		else {
			this.accountSelected = a;
			String[] values = a.toArray();
			this.fieldTitle.setText(a.getTitle());
			for(int i=0; i<values.length; i++) this.arrayAccountField[i].setText(values[i]);
			this.setFieldEditable(true);
			this.list.setSelectedValue(a,true);
		}
	}

	public void setFieldEditable(boolean b)
	{
		this.labelTitle.setEnabled(b);
		this.labelDescription.setEnabled(b);
		this.fieldTitle.setEditable(b);
		this.buttonScore.setEnabled(b);
		this.buttonDescription.setEnabled(b);
		for(JComponent component : this.arrayAccountField) component.setEnabled(b);
		for(JComponent component : this.arrayAccountLabel) component.setEnabled(b);
		for(JComponent component : this.arrayClipButton) component.setEnabled(b);
	}

	public void setFunctionUsable(boolean b)
	{
		this.list.setEnabled(b);
		this.buttonNew.setEnabled(b);
		this.buttonDelete.setEnabled(b);
	}

	public void reflectInputs()
	{
		Account account = this.accountSelected;
		if(account == null) return;

		account.setTitle(this.fieldTitle.getText());
		String text = null;
		for(int i=0; i<this.arrayAccountField.length; i++) {
			text = Master.this.arrayAccountField[i].getText();
			account.setValueAt(i,text);
		}
		this.person.fireListDataEvent();
	}

	public void fireFirstProcess()
	{
		super.setVisible(true);
	}

	class MenuItemListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			String command = e.getActionCommand();
			switch(command) {
			case "file_new":
				Master.this.masterpass = null;
				Master.this.setFile(null);
				Master.this.setPerson(new Person());
				break;
			case "file_load":
				IODialog dialogLoad = new IODialog(Master.this,IODialog.MODE_LOAD);
				dialogLoad.setVisible(true);
				String pathLoad = dialogLoad.getPath();
				String mpLoad = dialogLoad.getMasterPassword();
				if(pathLoad == null || mpLoad == null) return;
				try {
					File file = new File(pathLoad);
					Person p = PersonIO.load(file,mpLoad);
					Master.this.masterpass = mpLoad;
					Master.this.setFile(file);
					Master.this.setPerson(p);
				}
				catch(IOException ioe) {
					ioe.printStackTrace();
				}
				break;
			case "file_save":
				if(Master.this.person instanceof NullPerson) return;
				if(Master.this.file == null) {
					IODialog dialogSave = new IODialog(Master.this,IODialog.MODE_SAVE);
					dialogSave.setVisible(true);
					String pathSave = dialogSave.getPath();
					String mpSave = dialogSave.getMasterPassword();
					if(pathSave == null || mpSave == null) return;
					try {
						File file = new File(pathSave);
						PersonIO.save(file,mpSave,person);
						Master.this.masterpass = mpSave;
						Master.this.setFile(file);
					}
					catch(IOException ioe) {
						ioe.printStackTrace();
					}
				}
				else {
					try {
						PersonIO.save(Master.this.file,Master.this.masterpass,person);
					}
					catch(IOException ioe) {
						ioe.printStackTrace();
					}
				}
				break;
			case "file_saveas":
				if(Master.this.person instanceof NullPerson) return;
				IODialog dialogSave = new IODialog(Master.this,IODialog.MODE_SAVE);
				dialogSave.setVisible(true);
				String pathSave = dialogSave.getPath();
				String mpSave = dialogSave.getMasterPassword();
				if(pathSave == null || mpSave == null) return;
				try {
					File file = new File(pathSave);
					PersonIO.save(file,mpSave,person);
					Master.this.masterpass = mpSave;
					Master.this.setFile(file);
				}
				catch(IOException ioe) {
					ioe.printStackTrace();
				}
				break;
			case "file_quit":
				System.exit(0);
				break;
			case "config_settings":
				String msgSettings = "Coming soon";
				String titleSettings = "Settings";
				JOptionPane.showMessageDialog(Master.this,msgSettings,titleSettings,JOptionPane.INFORMATION_MESSAGE);
				break;
			case "config_about":
				String msgAbout = "Password manager 1.0(beta)" + System.lineSeparator() + "Created by SeRectifier, 2022.04";
				String titleAbout = "About";
				JOptionPane.showMessageDialog(Master.this,msgAbout,titleAbout,JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}

	class ClipBoardListener implements ActionListener
	{
		protected int command = -1;

		public ClipBoardListener(int c)
		{
			this.command = c;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			String text = Master.this.accountSelected.toArray()[this.command];
			Toolkit kit = Toolkit.getDefaultToolkit();
			Clipboard cb = kit.getSystemClipboard();
			StringSelection ss = new StringSelection(text);
			cb.setContents(ss,ss);
		}
	}

	class FunctionListener implements ActionListener,ListSelectionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			String command = e.getActionCommand();
			switch(command) {
			case "function_new":
				Account account = new Account();
				Master.this.person.addAccount(account);
				Master.this.setAccountSelected(account);
				break;
			case "function_delete":
				int index = Master.this.list.getSelectedIndex();
				Master.this.person.removeAccountAt(index);
				Master.this.setAccountSelected(null);
				break;
			}
		}

		@Override
		public void valueChanged(ListSelectionEvent e)
		{
			Account account = Master.this.list.getSelectedValue();
			Master.this.setAccountSelected(account);
		}
	}

	class EditListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			String command = e.getActionCommand();
			switch(command) {
			case "edit_description":
				SimpleTextDialog dialog = new SimpleTextDialog(Master.this,"Edit description");
				dialog.setEditingText(Master.this.accountSelected.getDescription());
				dialog.setVisible(true);
				String res = dialog.getResult();
				if(res != null) Master.this.accountSelected.setDescription(res);
				break;
			case "edit_score":
				Master.this.reflectInputs();
				break;
			}
		}
	}

	class QuitListener extends WindowAdapter implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}

		@Override
		public void windowClosing(WindowEvent e)
		{
			System.exit(0);
		}
	}
}
