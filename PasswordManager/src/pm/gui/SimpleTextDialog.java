package pm.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class SimpleTextDialog extends JDialog
{
	private static final long serialVersionUID = -5387660170817829822L;

	protected String result = null;

	private final JTextArea area = new JTextArea();
	private final JButton buttonCancel = new JButton("Cancel");
	private final JButton buttonDone = new JButton("Done");

	public SimpleTextDialog(JFrame owner,String title)
	{
		super(owner,title,true);

		this.initField();
		this.initListener();
		this.initPane();

		super.setSize(new Dimension(300,300));
		super.setResizable(true);
	}

	public SimpleTextDialog(JDialog owner,String title)
	{
		super(owner,title,true);

		this.initField();
		this.initListener();
		this.initPane();

		super.setSize(new Dimension(300,300));
		super.setResizable(true);
	}

	private void initField()
	{
		this.buttonCancel.setActionCommand("button_cancel");
		this.buttonDone.setActionCommand("button_done");
	}

	private void initListener()
	{
		Listener listener = new Listener();
		this.buttonCancel.addActionListener(listener);
		this.buttonDone.addActionListener(listener);
		super.addWindowListener(listener);
	}

	private void initPane()
	{
		JPanel paneSouth = new JPanel(new FlowLayout());
		paneSouth.add(this.buttonCancel);
		paneSouth.add(this.buttonDone);

		Border borderOuter = BorderFactory.createMatteBorder(4,4,4,4,super.getBackground());
		Border borderInner = BorderFactory.createEtchedBorder();
		this.area.setBorder(BorderFactory.createCompoundBorder(borderOuter,borderInner));

		super.add(this.area,BorderLayout.CENTER);
		super.add(paneSouth,BorderLayout.SOUTH);
	}

	public void setEditingText(String t)
	{
		this.area.setText(t);
	}

	public String getResult()
	{
		return this.result;
	}

	class Listener extends WindowAdapter implements ActionListener
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			SimpleTextDialog.this.result = null;
			SimpleTextDialog.super.dispose();
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			String command = e.getActionCommand();
			switch(command) {
			case "button_cancel":
				SimpleTextDialog.this.result = null;
				break;
			case "button_done":
				SimpleTextDialog.this.result = SimpleTextDialog.this.area.getText();
				break;
			}
			SimpleTextDialog.super.dispose();
		}
	}
}
