package pm.run;

import java.awt.image.*;
import java.io.*;

import javax.imageio.*;
import javax.swing.*;
import pm.gui.*;

public final class Main
{
	public static void main(String[] args)
	{
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (InstantiationException e) {
			e.printStackTrace();
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		BufferedImage icon = null;

		try {
			File dirClassPath = new File(System.getProperty("java.class.path"));
			File dirYard = dirClassPath.getParentFile();
			File fileIcon = new File(dirYard,"icon.png");
			icon = ImageIO.read(fileIcon);
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}

		Master master = new Master();
		master.setIconImage(icon);
		master.fireFirstProcess();
	}
}
