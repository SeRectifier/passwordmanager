# Password manager
This small program manages your accounts (e.g. music streaming service, shopping site or other web services).
You need to remember only one master password. Your secret data will be stored in XOR-encrypted file.
